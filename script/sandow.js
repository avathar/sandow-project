function Sandow(startDate, initialRepeat, step, days, maxQuantity) {
	this.initialRepeat = initialRepeat; //начальное количество повторов
	this.step = step; // на сколько повторов прибавляется за шаг
	this.days = days; // через сколько дней шаг (интервал приращения)
	this.startDate = startDate; //начальная дата
	this.maxQuantity = maxQuantity; // максимальное количество повторов
	this.calculateRepeat = 0;
}

Sandow.prototype = {
	nowRepeat: function () {
		//метод возвращающий количество повторов на сегодня. При выполнеии записывает данные в свойство calculateRepeat, чтоб избежать повторов пересчета
		let startDay = new Date(this.startDate); // начальный день
		let nowDay = new Date(); //сегодняшний день

		function diffDates(now, initial) {
			// функция возвращает разницу в днях (не целых, так как даты вычитаются в милисекундах)
			return (now - initial) / (60 * 60 * 24 * 1000); //
		}
		let delta = Math.floor(diffDates(nowDay, startDay)); //округляем дни в меньшую сторону
		let steps = Math.floor(delta / this.days); //вычисляем сколько шагов приращения сделано за это время

		this.calculateRepeat =
			this.initialRepeat + steps * this.step < this.maxQuantity
				? this.initialRepeat + steps * this.step
				: this.maxQuantity;
		return this.calculateRepeat; //используя тернарную форму, возвращаем либо текущее количество повторений,
	} //либо максимальное количество, если оно достигнуто
};
let sandowArray; //иннициализирую массив упражнений
let prog = 'id7'; //инициализирую переменную содержащую программу по умолчанию
let start = '2020-04-14'; //начальная дата по умолчанию
let view; //инициализирую объект представления
let exercise = 1; //переменная содержащая текущее упражнение
let style = 'firststyle'; // стиль отображения по умолчанию

sandowArrayInit(); //первый рендеринг

function sandowArrayInit(reinit) {
	//проверяю есть ли настройки в локальном хранилище
	if (localStorage.sandowstart) {
		start = localStorage.sandowstart;
	}
	if (localStorage.sandowprog) {
		prog = localStorage.sandowprog;
	}
	if (localStorage.sandowstyle) {
		style = localStorage.sandowstyle;
	}
	sandowArray = []; //опустошил массив упражнений
	for (let i = 0; i < exercisesArray(prog).length; i++) {
		//создаю объект для каждого упражнения
		sandowArray[i] = new Sandow(
			start,
			exercisesArray(prog)[i][0],
			exercisesArray(prog)[i][1],
			exercisesArray(prog)[i][2],
			exercisesArray(prog)[i][3]
		);
		sandowArray[i].nowRepeat(); //тут вызывается метод, который рассчитывает текущее количество повторений и кладёт его в свойство calculateRepeat
	}
	if (reinit === 'reinit') {
		// при повторном рендеринге удаляем старые закреплённые слушатели событий и обнуляем объект представления
		view.removeEventListeners();
		view = null;
	}
	view = new View(); // создаю объект представления
	//	view.consLog();
	view.changeStyle();
	view.onChangeStartDateBehav();
	view.initProgramChanger();
	view.initStyleChanger();
	view.initProgramDescription();
	view.onChangeProgramBehav();
	view.displayExercises();
	view.onClickExerciseListener();
	view.onChangeExercise();
	view.onChangeStyleBehav();
	view.keyboardListener();
}
