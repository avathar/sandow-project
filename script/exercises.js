//определяем массив в котором для каждого упражнения указано:
//начальное количество, шаг, интервал приращения и максимальное количество повторов

function exercisesArray(prog) {
	switch (prog) {
		case 'id1':
			return [
				[10, 1, 4, 120],
				[5, 1, 4, 53],
				[5, 1, 4, 24],
				[4, 1, 6, 14],
				[4, 1, 6, 12],
				[10, 1, 4, 22],
				[6, 1, 6, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[5, 1, 6, 17],
				[5, 1, 6, 17],
				[2, 1, 15, 7],
				[5, 1, 4, 53],
				[3, 1, 15, 10],
				[3, 1, 15, 10],
				[3, 1, 4, 53],
				[10, 1, 4, 53]
			];
		case 'id2':
			return [
				[10, 1, 4, 120],
				[5, 1, 4, 53],
				[5, 1, 4, 24],
				[4, 1, 6, 14],
				[4, 1, 6, 12],
				[10, 1, 4, 22],
				[6, 1, 6, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[5, 1, 6, 17],
				[5, 1, 6, 17],
				[2, 1, 15, 7],
				[6, 1, 4, 53],
				[3, 1, 15, 10],
				[3, 1, 15, 10],
				[3, 1, 4, 53],
				[10, 1, 4, 53]
			];
		case 'id3':
			return [
				[10, 1, 4, 120],
				[5, 1, 4, 53],
				[5, 1, 4, 24],
				[4, 1, 6, 14],
				[4, 1, 6, 12],
				[10, 1, 4, 22],
				[6, 1, 6, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[5, 1, 6, 17],
				[5, 1, 6, 17],
				[2, 1, 15, 7],
				[6, 1, 4, 53],
				[3, 1, 15, 10],
				[3, 1, 15, 10],
				[3, 1, 4, 53],
				[10, 1, 4, 53]
			];
		case 'id4':
			return [
				[15, 1, 4, 120],
				[8, 1, 4, 53],
				[6, 1, 4, 24],
				[6, 1, 6, 14],
				[4, 1, 6, 12],
				[10, 1, 4, 22],
				[8, 1, 6, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[5, 1, 6, 17],
				[5, 1, 6, 17],
				[2, 1, 15, 7],
				[8, 1, 4, 53],
				[3, 1, 15, 10],
				[3, 1, 15, 10],
				[3, 1, 4, 53],
				[15, 1, 4, 53]
			];
		case 'id5':
			return [
				[30, 1, 2, 120],
				[15, 1, 3, 53],
				[10, 1, 3, 24],
				[8, 1, 3, 14],
				[5, 1, 4, 12],
				[12, 1, 3, 22],
				[8, 1, 4, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[5, 1, 3, 17],
				[5, 1, 3, 17],
				[2, 1, 8, 7],
				[15, 1, 2, 53],
				[3, 1, 4, 10],
				[3, 1, 15, 10],
				[3, 1, 4, 53],
				[25, 1, 4, 53]
			];
		case 'id6':
			return [
				[20, 1, 2, 120],
				[10, 1, 3, 53],
				[7, 1, 3, 24],
				[7, 1, 3, 14],
				[4, 1, 4, 12],
				[10, 1, 3, 22],
				[8, 1, 4, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[5, 1, 3, 17],
				[5, 1, 3, 17],
				[2, 1, 8, 7],
				[10, 1, 4, 53],
				[3, 1, 4, 10],
				[3, 1, 8, 10],
				[3, 1, 4, 53],
				[20, 1, 4, 53]
			];

		default:
			return [
				[50, 5, 1, 120],
				[25, 2, 1, 53],
				[10, 1, 1, 24],
				[10, 1, 4, 14],
				[5, 1, 3, 12],
				[15, 1, 3, 22],
				[10, 1, 3, 17],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[120, 1, 1, 120],
				[10, 1, 3, 17],
				[10, 1, 3, 17],
				[3, 1, 4, 7],
				[25, 2, 1, 53],
				[3, 1, 3, 10],
				[3, 1, 3, 10],
				[3, 1, 4, 53],
				[25, 2, 1, 53]
			];
	}
}
