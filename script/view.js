function View() {
	this.listeningEvents = []; //массив где хранятся слушатели событий
}
View.prototype = {
	//метод вывода сообщений в консоль
	consLog: function () {
		for (let j = 0; j < sandowArray.length; j++) {
			console.log(
				`Упражнение №${j + 1}: ${sandowArray[j].calculateRepeat} повторов из ${sandowArray[j].maxQuantity}`
			);
		}
	},
	// метод для изменения даты начала занятий
	onChangeStartDateBehav: function () {
		let startDateChanger = document.getElementById('beginDate'); //выбор элемента ввода даты
		startDateChanger.value = start; // заполняю элемент нужной датой начала
		startDateChanger.addEventListener('change', onChangeStartDate); //закрепляю слушатель на изменение даты
		this.listeningEvents.push([startDateChanger, 'change', onChangeStartDate]); //добавляю слушатель в массив
		function onChangeStartDate() {
			//при изменении даты
			let dateNow = new Date();
			let dd = dateNow.getDate(); //получаю сегодняшний день в двузначном формате
			if (dd < 10) dd = '0' + dd;
			let mm = dateNow.getMonth() + 1; // получаю сегодняшний месяц в двузначном формате
			if (mm < 10) mm = '0' + mm;
			let startText = `${dateNow.getFullYear()}-${mm}-${dd}`; //составляю строку в формате ГГГГ-ММ-ДД
			// проверяю если выбраная дата больше сегодняшней, то ставлю сегодняшнюю, если меньше, ставлю выбранную
			if (new Date(startDateChanger.value) > Date.now()) {
				start = startText;
			} else {
				start = startDateChanger.value;
			}
			localStorage.sandowstart = start; // сохраняю дату в локальное хранилище
			sandowArrayInit('reinit'); //запускаю перерисовку страницы
		}
	},
	// метод для изменения программы занятий
	onChangeProgramBehav: function () {
		let programChanger = document.getElementById('program'); //выбор элемента списка
		programChanger.addEventListener('change', onChangeProgram); //закрепление слушателя на список, на его изменение
		this.listeningEvents.push([programChanger, 'change', onChangeProgram]);
		//при изменении списка
		function onChangeProgram() {
			prog = programChanger.value; //сохраняю новую программу в переменную
			localStorage.sandowprog = prog; // и в локальное хранилище
			sandowArrayInit('reinit'); //запускаю перерисовку страницы
		}
	},
	// метод отображающий список упражнений
	displayExercises: function () {
		//определение ul в который будет выведен список упражнений
		let resultField = document.getElementById('resultField');
		//обнуление содержимого
		resultField.innerHTML = '';
		//вывод списка в выбранный ul
		for (let j = 0; j < sandowArray.length; j++) {
			resultField.innerHTML =
				resultField.innerHTML +
				`<li class="exerciseString" id=${j + 1}>Упражнение №${j + 1}: ${
					sandowArray[j].calculateRepeat
				} повторов из ${sandowArray[j].maxQuantity}</li>`;
		}
		// помечаем текущее упражнение css классом
		document.getElementsByClassName('exerciseString')[exercise - 1].classList.add('markedclass');
	},
	//метод изменения стиля
	changeStyle: function () {
		let styleEl = document.getElementById('main'); // ищем элемент стиля всей страницы
		let classArray = styleEl.classList; //получаем стили, которые ему присвоены
		//нас интересуют только стили firststyle, secondstyle и thirdstyle
		// перебираем стили чтоб их найти, если находим то удаляем, и назначаем стиль который выбрали
		if (classArray.length > 0) {
			for (let i = 0; i < classArray.length; i++) {
				if (classArray[i] === 'firststyle') {
					styleEl.classList.remove('firststyle');
					styleEl.classList.add(style);
				} else if (classArray[i] === 'secondstyle') {
					styleEl.classList.remove('secondstyle');
					styleEl.classList.add(style);
				} else if (classArray[i] === 'thirdstyle') {
					styleEl.classList.remove('thirdstyle');
					styleEl.classList.add(style);
				} else {
					styleEl.classList.add(style);
				}
			}
		} else styleEl.classList.add(style); //если не находим, то назначаем свой стиль
	},
	// метод для реакции на клик по упражнению
	onClickExerciseListener: function () {
		//определение всех элементов списка упражнений по классу
		let liSelect = document.getElementsByClassName('exerciseString');
		for (let index = 0; index < liSelect.length; index++) {
			//перебираем все найденые элементы
			liSelect[index].addEventListener('click', onExerciseClick); //на каждый элемент прикрепляем слушатель
			this.listeningEvents.push([liSelect[index], 'click', onExerciseClick]); //и заносим слушатель в массив
		}
		let that = this; //так как в функции потеряется контекст, то создаю для него переменную
		function onExerciseClick(eventObject) {
			//если кликнули по строчке
			exerciseList = document.getElementsByClassName('exerciseString'); //перебираю все строчки
			for (let i = 0; i < exerciseList.length; i++) {
				exerciseList[i].classList.remove('markedclass'); // удаляю с них маркер markedclass
			}

			let clickedExercise = eventObject.currentTarget;
			clickedExercise.classList.add('markedclass'); // и назначаю маркер на ту строчку от которой произошло событие клика
			exercise = Number(clickedExercise.id); // назначаю номер текущего упражнения в глобальную переменную
			that.onChangeExercise(); // и воспользовавшись сохранённым выше контекстом вызываю метод перерисовки блока с упражнением
		}
	},
	//метод для удаления слушателей перед перерисовкой страницы
	//просто перечисляем все элементы массива и удаляем слушателей
	removeEventListeners: function () {
		for (let index = 0; index < this.listeningEvents.length; index++) {
			var el = this.listeningEvents[index];
			//в массив помещал слушателей в таком порядке:
			//объект, событие, функция
			//так же по порядку и достаю
			el[0].removeEventListener(el[1], el[2]);
		}
	},
	//метод перерисовки блока с упражнением
	onChangeExercise: function () {
		//нахожу на странице нужные блоки
		let nowRepeatField = document.getElementById('nowrepeat');
		let imageField = document.getElementById('imageField');
		let textField = document.getElementById('textField');
		nowRepeatField.innerHTML = sandowArray[exercise - 1].calculateRepeat; //заполняю блок с текущим количеством
		imageField.innerHTML = ''; //очищаю блок с картинкой
		textField.innerHTML = ''; // и с текстом
		//так как оставил возможность хранения нескольких картинок, то перечисляю каждый массивчик с картинками и вставляю в страницу
		for (let j = 0; j < complexArray()[exercise - 1][0].length; j++) {
			imageField.innerHTML =
				imageField.innerHTML + `<img class="exerciseImg" src="${complexArray()[exercise - 1][0][j]}"></img>`;
		}
		//так же и с тектовыми строками
		for (let j = 0; j < complexArray()[exercise - 1][1].length; j++) {
			textField.innerHTML =
				textField.innerHTML + `<span class="exerciseText" >${complexArray()[exercise - 1][1][j]}</span><br/>`;
		}
		//тут надо проверить, если прошло больше чем полгода с начала упражнений, то надо сообщить, что пора увеличивать вес
		let dateNow = new Date();
		let startDay = new Date(start);
		// пользуясь встроенным классом Data получаю дату из моей строковой переменной и текущую
		// вычисляю приблизительно, если есть полгода между ними то вставляю в страницу сообщение
		if (dateNow - startDay > (60 * 60 * 24 * 1000 * 365) / 2) {
			textField.innerHTML =
				textField.innerHTML +
				`<span class="exerciseTextWarning" >Увеличьте вес гантелей на 1кг и начните программу заново!</span><br/>`;
		}
	},
	//метод для установки селектора списка программ в соответствии с текущей программой
	initProgramChanger: function () {
		let programChanger = document.querySelector('#program');
		programChanger.value = prog;
	},
	//метод для установки селектора списка стилей в соответствии с текущим стилем
	initStyleChanger: function () {
		let styleChanger = document.getElementById('changestyle');
		styleChanger.value = style;
	},
	//метод для вывода описания программы тренировок
	initProgramDescription: function () {
		let programDescr = document.getElementById('programDescription'); //нахожу блок куда выводить
		programDescr.innerHTML = ''; //опустошаю его
		//и заполняю перебирая массив строк
		for (let i = 0; i < programDescription(prog).length; i++) {
			programDescr.innerHTML = programDescr.innerHTML + `<span >${programDescription(prog)[i]}</span><br />`;
		}
	},
	//метод для изменения стиля
	onChangeStyleBehav: function () {
		let styleChanger = document.getElementById('changestyle'); //выбор элемента списка
		styleChanger.addEventListener('change', onChangeStyle); //закрепление слушателя на список, на его изменение
		this.listeningEvents.push([styleChanger, 'change', onChangeStyle]); //и сразу добавляю его в массив слушателей
		function onChangeStyle() {
			style = styleChanger.value; //когда изменяется выбранный стиль, то присваиваю новый стиль переменной
			localStorage.sandowstyle = style; // сохраняю его в локальном хранилище
			sandowArrayInit('reinit'); //запускаю перерисовку страницы
		}
	},
	// метод для прослушивания нажатий на пробел
	keyboardListener: function () {
		document.addEventListener('keydown', onKeydownSandow); //назначаю слушателя
		this.listeningEvents.push([document, 'keydown', onKeydownSandow]); //помещаю его в массив
		let that = this; //для сохранения контекста this
		function onKeydownSandow(event) {
			if (event.code == 'Space') {
				//если нажат пробел, то увеличить счётчик упражнений,
				//если он стал больше количества упражнений, то приравнять единице
				if (exercise > sandowArray.length - 1) {
					exercise = 1;
				} else {
					exercise++;
				}

				onExerciseSelect();
			}
			//функция маркирует текущее упражнение в списке классом
			function onExerciseSelect() {
				//удаляем маркировку со всех строк, для этого перебираем все строки из массива строк
				let exerciseList = document.getElementsByClassName('exerciseString');
				for (let i = 0; i < exerciseList.length; i++) {
					exerciseList[i].classList.remove('markedclass'); //удаляем
				}

				let selectExercise = exerciseList[exercise - 1]; //теперь выбираем строку из массива с номером как упражнение
				selectExercise.classList.add('markedclass'); //и маркируем
				that.onChangeExercise(); //вызываем метод для перерисовки
			}
		}
	}
};
